#!/usr/bin/perl

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.

eval
{
 require '/opt/bamt/common.pl';
 require '/opt/bamt/poolstats.pl';
 require '/opt/bamt/libfixer.pl';
};

if ($@)
{
  die "\n\n\nWe were unable to find or process a core BAMT library:\n\n" .
    $@ . "\n\n\nThe BAMT tools cannot function until the above error is corrected.\n\nMaybe this happened";
}


$SIG{__DIE__} = sub { endwin; &handleDeath(@_); };     


use Socket;
use IO::Handle;
use Term::ReadKey; 
use Curses;
use LWP::Simple;
use threads;
use threads::shared;


$ENV{DISPLAY}=':0.0';

our $conf = &getConfig;
%conf = %{$conf};

our %poolstats :shared;
our $doingPoolUpdate :shared;
our $wanttodie :shared;
our $myip = &getIPstring;
our $lastupdatecheck = 0;
our $updatemsg = &checkUpdates;
our $subdisplay = 0;
our @history;
our $historygpu = -1;
our $historygoesdown = 1;

$wanttoddie = 0;

my $puthread = threads->create('pu_thread'); 
$puthread->detach();

initscr;
start_color();

init_pair(1, COLOR_GREEN, COLOR_BLACK);
init_pair(2, COLOR_YELLOW, COLOR_BLACK);
init_pair(3, COLOR_RED, COLOR_BLACK);
init_pair(4, COLOR_BLACK, COLOR_WHITE);



my @lasttemp;
my @lastfan;

my $ccount = 0;
my $checkcount = 180;

while(1)
{
 $ccount++;
 
 if (! $ccount % 10)
 {
 	# $myip = &getIPstring;
 	$myip = "test";
 }
 
 if ($ccount > $checkcount)
 {
 	$ccount = 0;
 }
 
 my ($xsize,$ysize) = GetTerminalSize(); 
 #clear();
 
 for (my $i = 1;$i<$ysize;$i++)
 {
 	 addstr($i,0,sprintf("%-" . $xsize . "s",""));
 }
 
 my $ts = sprintf("%-" . $xsize . "s", localtime() . "  -  " . $conf{'settings'}{'miner_id'} . "  -  $myip");

 
 attron(COLOR_PAIR(4));
 addstr(0,0,$ts);
 attroff(COLOR_PAIR(4));

 addstr(2,0, "   Temp    Fan% (rpm)    Load  Rate     Accept/Invalid   Status");

 my $gpu = 0;
 my $delay = 10;
 my $gotdefault = 0;


 my $tothash = 0;
 my $totaccept = 0;
 my $totinvalid = 0;

 my $histmp = {};

 my @gpus = &getFreshGPUData;

 for ($gpu = 0;$gpu < @gpus;$gpu++)
 {
        my $ls = "";
        my $errtxt = "";

	    my $gc = $conf{'gpu'.$gpu};

	    $histmp{'gpu' . $gpu . '_rate'} = $gpus[$gpu]{'hashrate'};
	    $histmp{'gpu' . $gpu . '_temp'} = $gpus[$gpu]{'current_temp_0'};
	    $histmp{'gpu' . $gpu . '_fanrpm'} = $gpus[$gpu]{'fan_rpm'};
	    $histmp{'gpu' . $gpu . '_load'} = $gpus[$gpu]{'current_load'};
	    $histmp{'gpu' . $gpu . '_accepted'} = $gpus[$gpu]{'shares_accepted'};
	    $histmp{'gpu' . $gpu . '_invalid'} = $gpus[$gpu]{'shares_invalid'};
	    
        $state = 0;        
        $ls .= "$gpu: ";
       
        my $tmp = $gpus[$gpu]{'current_temp_0'};

        my $ts = $tmp . 'c';	

        if ($tmp > @lasttmp[$gpu])
        {
          $ts .= "+";
        }
        elsif ($tmp < @lasttmp[$gpu])
        {
         $ts .= "-";
        }

        $ls .= sprintf("%-8s",$ts);

        @lasttmp[$gpu]=$tmp;

        if ($tmp > ${$gc}{'monitor_temp_hi'})
        {
         $state++;
         $errtxt .= " High Temp";
        }
        elsif ($tmp < ${$gc}{'monitor_temp_lo'})
        {
         $state++;          
 	 $errtxt .= " Low Temp";
        }
       

	$ts = $gpus[$gpu]{'fan_speed'} . '% (' . $gpus[$gpu]{'fan_rpm'};
        
        if ($gpus[$gpu]{'fan_rpm'} > @lastfan[$gpu])
        {
         $ts .= '+';
        }
        elsif ($gpus[$gpu]{'fan_rpm'} < @lastfan[$gpu])
        {
         $ts .= '-';
        }
      
	@lastfan[$gpu] = $gpus[$gpu]{'fan_rpm'};
	
        $ts .= ')';
        $ls .= sprintf("%-13s",$ts); 


        $ls .= sprintf("%3d\%   ",$gpus[$gpu]{'current_load'});

        if ($gpus[$gpu]{'current_load'} < ${$gc}{'monitor_load_lo'})
        {
         $state++;
         $errtxt .= " Low Load"
        }

	if ($gpus[$gpu]{'hashrate'} < ${$gc}{'monitor_hash_lo'})
        {
         $state++;
         $errtxt .= " Low Mhs";
        }

        $tothash += $gpus[$gpu]{'hashrate'};
        $totaccept += $gpus[$gpu]{'shares_accepted'};
        $totinvalid += $gpus[$gpu]{'shares_invalid'};

        $ls .= sprintf("%-9s", sprintf("%3.0d",$gpus[$gpu]{'hashrate'}) . " Mhs" );

        $ts = $gpus[$gpu]{'shares_accepted'} . "/" . $gpus[$gpu]{'shares_invalid'} . " ";

        if ($gpus[$gpu]{'shares_accepted'} > 0)
        {
                $ts .= sprintf("(%-2.2f%)", $gpus[$gpu]{'shares_invalid'}/($gpus[$gpu]{'shares_accepted'} + $gpus[$gpu]{'shares_invalid'})*100 );
        }

	$ls .= sprintf("%-16s",$ts);

        my $url = $gpus[$gpu]{'pool_url'};

        if ($url =~ m/.+\@(.+)/)
        {
          $url = $1;
	  if ($url =~ m/(.+):.*/)
	  {
		$url = $1;
          }
        }

        if ($state > 0)
        {
         $ls .= $errtxt;
        }
        else
        {
         $ls .= $errtxt;
         $ls .= " $url";
        }
        
        if ($state == 0)
        {
                attron(COLOR_PAIR(1));
                addstr($gpu + 3,0,$ls);
                attroff(COLOR_PAIR(1));
		$delay = 10;
        } 
        elsif ($state == 1)
        {
                attron(COLOR_PAIR(2));
                addstr($gpu + 3,0,$ls);
                attroff(COLOR_PAIR(2));
		$delay = 5;
        }
        else
        {
                attron(COLOR_PAIR(3));
                addstr($gpu + 3,0,$ls);
                attroff(COLOR_PAIR(3));
		$delay = 2;
        }

  }
  
 if ($doingPoolUpdate)
 {
  $delay = 2;
 }
 

 my $ts = "Total: $tothash Mhash/s  $totaccept accepted, $totinvalid invalid ";

 if ($totaccept > 0)
 {
  $ts .= sprintf("(%-2.2f%)  ", $totinvalid/($totaccept + $totinvalid)*100 );
 }

 addstr($gpu + 4,0,$ts);

 $histmp{'totalhash'} = $tothash;
 
 if (localtime() =~ m/.*(\d\d\:\d\d\:\d\d).*/)
 {
  $histmp{'time'} = $1;
 }

 if ($historygoesdown)
 {
 	push(@history, { %histmp });
 
	while (@history > ($ysize - $gpu - 9))
	{
	  	shift(@history);
 	}
 }
 else
 {
   	unshift(@history, { %histmp });
 
        while (@history > ($ysize - $gpu - 9)) 
        {
                pop(@history);
        }
 }
 


 if ($subdisplay == 0)
 {
  drawHistory($gpu + 6, $gpu);
 }
 elsif ($subdisplay == 1)
 {
  drawPoolStats($gpu + 6);
 }

 
 my $tmpago = "checked ";
 my $tmptime = time - $lastupdatecheck;
 if ($tmptime < 60) { $tmpago .= $tmptime . " seconds ago"; }
 elsif ($tmptime < 120) { $tmpago .= "a minute ago"; }
 else { $tmpago .= int($tmptime / 60) . " minutes ago"; }
 
 # check updates twice a day
 if ($tmptime > 43200) { $updatemsg = &checkUpdates; }
 
 $ts = sprintf("%-" . $xsize . "s", " ? for help  |  " . $updatemsg . " (" . $tmpago . ")"  );

 attron(COLOR_PAIR(4));
 addstr($ysize - 1,0,$ts);
 attroff(COLOR_PAIR(4));
 refresh;

 
 my $key = ReadKey($delay);

 if (defined($key))
 {
	 &processKey($key);
 }
}

endwin;



sub pu_thread
{
 
 while(!$wanttodie)
 {
  $doingPoolUpdate = 1;
  &updatePoolStats; 
  $doingPoolUpdate = 0;
  sleep(90);
 }
}


sub processKey
{
 my ($key) = @_;

 if ($key eq 'q')
 {
  endwin;
  exit(0);
 }

 if ((ord($key) > 47) && (ord($key) < 58))
 {
 	 if ( (defined(${$conf}{'gpu'.$key}) ) && (! ${$conf}{'gpu'.$key}{'disabled'}) )
 	 {
 	 	 endwin;
 	 	 
 	 	 if (${$conf}{'gpu'.$key}{'cgminer'})
 	 	 {
 	 	 	 system("screen -r cgminer");
 	 	 }
		 if (${$conf}{'gpu'.$key}{'phoenix2'})
                 {
                         system("screen -r phoenix2");
                 }
 	 	 else
 	 	 {
 	 	 	 system("screen -r gpu$key"); 
 	 	 }
 	 	 
 	 	 initsrc;
 	 }
 }
 elsif ($key eq '`')
 {
  $historygpu = -1;
 }
 elsif ($key eq ')')
 {
 	 $historygpu = 0;
 }
 elsif ($key eq '!')
 {
 	 $historygpu = 1;
 }
 elsif ($key eq '@')
 {
 	 $historygpu = 2;
 }
 elsif ($key eq '#')
 {
 	 $historygpu = 3;
 }
 elsif ($key eq '$')
 {
 	 $historygpu = 4;
 }
 elsif ($key eq '%')
 {
 	 $historygpu = 5;
 }
 elsif ($key eq '^')
 {
 	 $historygpu = 6;
 }
 elsif ($key eq '&')
 {
 	 $historygpu = 7;
 }
 elsif ($key eq '*')
 {
 	 $historygpu = 8;
 }
 elsif ($key eq '(')
 {
 	 $historygpu = 9;
 }
 elsif ($key eq 'R')
 {
  endwin;
  system("/etc/init.d/mine restart");
  initscr;
 }
 elsif ($key eq 'S')
 {
  endwin;
  system("/etc/init.d/mine start");
  initscr;
 }
 elsif ($key eq 'D')
 {
  endwin;
  system("/etc/init.d/mine stop");
  initscr;
 }
 elsif ($key eq 'p')
 {
  endwin;
  system("nano /etc/bamt/pools");
  initscr;
 }
 elsif ($key eq 'c')
 {
  endwin;
  system("nano /etc/bamt/bamt.conf");
  initscr;
 }
 elsif ($key eq 'h')
 {
   if ($subdisplay == 0)
   {
   	   $subdisplay = 1;
   }
   else
   {
   	   $subdisplay = 0;
   }
   
 }
 elsif ($key eq 'o')
 {
   if ($historygoesdown == 0)
   {
           $historygoesdown = 1;
   }
   else
   {
           $historygoesdown  = 0;
   }

   @history = reverse(@history);

 }
 elsif ($key eq 'f')
 {
  $updatemsg = &checkUpdates;
 }
 elsif ($key eq 'F')
 {
  endwin;
  print "Running fixer...\n\n";
  system("/opt/bamt/fixer");
  print "\nFixer complete, press enter to continue...";
  my $w = ReadKey(0);
  $updatemsg = &checkUpdates;
 }
 elsif ($key eq '?')
 {
  endwin;
  
  print qq%
Pressing various keys in gpumon causes various things to happen.
Note the case, command keys are case sensitive.

Pressing 0 through 9 will open the screen session for Phoenix on the 
corresponding GPU.  Shift-0 through Shift-9 will display history for
that GPU.  Pressing ` will return to the history overview.

Controlling mining processes:       Managing BAMT on this miner:         

R - restart mining                  c - edit /etc/bamt/bamt.conf
S - start mining                    p - edit /etc/bamt/pools
D - stop mining                     f - check for updates
                                    F - run the fixer to apply updates
Other:

h - toggle between history and pool sub displays
o - toggle first/last order for history
space - refresh screen
? - list help for keys
q - quit
  %;
  
  print "\n\n[Press enter to continue...]";
  my $w = ReadKey(0);
 }
}

sub drawHistory
{
 my ($pos,$gpus) = @_;
 

 	 
 if (@history)
 {
 	 if ($historygpu < 0)
 	 {
 	 	  my $l = sprintf("%-8s %9s","Time","Total");
 
 	 	  for (my $k = 0;$k < $gpus;$k++)
 	 	  {
 	 	  	  $l .= sprintf("%8s","gpu$k");
 	 	  }
 
 	 	  addstr($pos,0, $l);
 	 	  $pos++;
 	 	 
		 for (my $i = 0;$i < @history;$i++)
		 {
			 my $l = sprintf("%-8s %9.2f", ${$history[$i]}{time}, ${$history[$i]}{totalhash});
			 
			 for (my $k = 0;$k < $gpus;$k++)
			 {
				 $l .= sprintf("%8.2f",${$history[$i]}{"gpu$k" . '_rate'});
			 }
			 
			 addstr($pos,0, $l);
			 $pos++;
		 }
	 }
	 else
	 {
	 	  my $l = sprintf("%-10s %8s %8s %8s %8s %8s %8s","GPU" . $historygpu . " Time","Hashrate","Temp","Fan RPM","Load","Accept","Invalid");
 
 	 	  addstr($pos,0, $l);
 	 	  $pos++;
 	 	  
 	 	  for (my $i = 0;$i < @history;$i++)
		  {
		  	   my $l = sprintf("%-10s %8.2f %7.2fc %8d %7d%% %8d %8d", ${$history[$i]}{time}, ${$history[$i]}{"gpu$historygpu" . '_rate'}, ${$history[$i]}{"gpu$historygpu" . '_temp'}, ${$history[$i]}{"gpu$historygpu" . '_fanrpm'}, ${$history[$i]}{"gpu$historygpu" . '_load'}, ${$history[$i]}{"gpu$historygpu" . '_accepted'}, ${$history[$i]}{"gpu$historygpu" . '_invalid'});
		  	   
		  	   addstr($pos,0, $l);
		  	   $pos++;
		  }
	 }
 }
 else
 {
 	 addstr($pos,0, "No history data.");
 }
 
}

sub drawPoolStats
{
 my ($pos) = @_;
 
 if (scalar keys %poolstats)
 {
   
  addstr($pos,0,"Pool:          Rate:       Confirmed:     Unconfirmed:   Estimated:");
  $pos++;

  my $totrate = 0;
  my $totconfirm = 0;
  my $totunconfirm = 0;
  my $totestimate = 0;

  foreach $pool (sort keys %poolstats)
  {
   my $ts = sprintf("%-15s",$pool);

   $ts .= sprintf("%-12.2f",$poolstats{$pool}{hash});
   if ($poolstats{$pool}{hash} =~ m/([\d\.]+)[+-]?/)
   {
    $totrate += $1;
   }

   $ts .= sprintf("%-15.8f",$poolstats{$pool}{confirm});
   if ($poolstats{$pool}{confirm} =~ m/([\d\.]+)[+-]?/)
   {
    $totconfirm += $1;
   }

   $ts .= sprintf("%-15.8f",$poolstats{$pool}{unconfirm});
   if ($poolstats{$pool}{unconfirm} =~ m/([\d\.]+)[+-]?/) 
   {
    $totunconfirm += $1;
   }

   $ts .= sprintf("%-15.8f",$poolstats{$pool}{estimate}); 
   if ($poolstats{$pool}{estimate} =~ m/([\d\.]+)[+-]?/)
   {
    $totestimate += $1;
   }

   addstr($pos,0,$ts);
   $pos++;
  }

  addstr($pos+1,0,"Totals:        " . sprintf("%-12.2f", $totrate) . sprintf("%-15.8f", $totconfirm) . sprintf("%-15.8f", $totunconfirm) . sprintf("%-15.8f", $totestimate) );
 }
 else
 {
 	  addstr($pos,0,"No pool api keys configured.");
 }
 
}



sub updatePoolStats
{

 foreach $pool (&get_poollist)
 {
  if (defined(${$conf}{'settings'}{"apikey_$pool"}))
  {

   my ($hash,$confirm,$unconfirm,$estimate) = &{"getPoolStats_$pool"}(${$conf}{'settings'}{"apikey_$pool"});  

   unless (defined($poolstats{$pool}))
   {
    $poolstats{$pool} = shared_clone( { hash => 0, confirm => 0, uncomfirm => 0, estimate => 0 });
   }
   
   if ($hash > $poolstats{$pool}{hash})
   {
    $poolstats{$pool}{hash} = "$hash+";
   }
   elsif ($hash < $poolstats{$pool}{hash})
   {
    $poolstats{$pool}{hash} = "$hash-";
   }
   else
   {
    $poolstats{$pool}{hash} = $hash;
   }
   
   if ($confirm > $poolstats{$pool}{confirm})
   {
    $poolstats{$pool}{confirm} = "$confirm+";
   }
   elsif ($confirm < $poolstats{$pool}{confirm})
   {
    $poolstats{$pool}{confirm} = "$confirm-";
   }
   else
   {
    $poolstats{$pool}{confirm} = $confirm;
   }


   if ($unconfirm > $poolstats{$pool}{unconfirm})
   {
    $poolstats{$pool}{unconfirm} = "$unconfirm+";
   }
   elsif ($unconfirm < $poolstats{$pool}{unconfirm})
   {
    $poolstats{$pool}{unconfirm} = "$unconfirm-";
   }
   else
   {
    $poolstats{$pool}{unconfirm} = $unconfirm;
   }


   if ($estimate > $poolstats{$pool}{estimate})
   {
    $poolstats{$pool}{estimate} = "$estimate+";
   }
   elsif ($estimate < $poolstats{$pool}{estimate})
   {
    $poolstats{$pool}{estimate} = "$estimate-";
   }
   else
   {
    $poolstats{$pool}{estimate} = $estimate;
   }

  }
 }	
}


sub getIPstring
{
  my %ips;
  my $interface;
  my $res = "";
  
 foreach ( qx{ (LC_ALL=C /sbin/ifconfig -a 2>&1) } ) 
 {
  $interface = $1 if /^(\S+?):?\s/;
  next unless defined $interface;
  $ips{$interface}->{STATE}=uc($1) if /\b(up|down)\b/i;
  $ips{$interface}->{IP}=$1 if /inet\D+(\d+\.\d+\.\d+\.\d+)/i;
 }

 for my $int ( keys %ips )
 {
	if (( $ips{$int}->{STATE} eq "UP" ) && defined($ips{$int}->{IP}) && !($int eq "lo"))
	{
		$res .= $int .":". $ips{$int}->{IP} . " ";
	}
 }
 
 return($res);
}


sub checkUpdates
{
	$lastupdatecheck = time;
	
	my $res = "Could not retrieve update list from network.";
	
	my $fixins = &checkFixesAvailable;
	
	if ($fixins)
	{
		if ($fixins == 1)
		{
			$res = "There is a BAMT fix available";
		}
		else
		{
			$res = "There are $fixins BAMT fixes available";
		}
	}
	else
	{
		$res = "BAMT is up to date";
	}
	
	return($res);
}

