#!/usr/bin/perl 

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.

eval
{
 require '/opt/bamt/common.pl';
 require '/opt/bamt/sendstatus.pl';
};

if ($@)
{
  die "\n\n\nWe were unable to find or process a core BAMT library:\n\n" .
    $@ . "\n\n\nThe BAMT tools cannot function until the above error is corrected.\n\nMaybe this happened";
}


$SIG{__DIE__} = sub { &handleDeath(@_); };


use YAML qw( DumpFile LoadFile );
use Email::Sender::Transport::SMTP;
use Email::Sender::Transport::SMTP::TLS;
use Email::Sender::Simple qw(sendmail);
use Email::Simple::Creator;
use Try::Tiny;
use Data::Dumper;
use Proc::PID::File;




our $verbose = 0;

our $momtmp = &getMomTmp;

our $alerts = 0;
our $recovery = 0;

%momtmp = %{$momtmp};

our $timesince = time - $momtmp{'lastrun'};

if (@ARGV)
{
 for my $o (@ARGV)
 {
   if (($o eq "-v") || ($o eq "--verbose"))
   {
     $verbose = 1;
     lg("mother starts ($timesince seconds since last run)");
   }
 }
}



my $conf = &getConfig;
%conf = %{$conf};

our $msg = "";
our $rmsg = "";

if (Proc::PID::File->running())
{
        # one at a time, gentlemen
	lg("Another mother is already running. Bail out!");
	exit(0);
}



if (!defined($conf{'settings'}{'autoconf_client'}) || ($conf{'settings'}{'autoconf_client'} > 0))
{
	if ($verbose)
	{
	  print "babysit autoconf client...\n";  
	}

	
	my $forkit = 0;
	if (-e '/var/run/autoconf_client.pid')
	{
		my $acpid = `cat /var/run/autoconf_client.pid`;
		if (kill 0, $acpid)
		{
			if ($verbose)
			{
				print "\tautoconf client is ok\n";  
			}
		}
		else
		{
			lg("\tstale pid file, spawning another client");  
			
			$forkit = 1;
		}
	}
	else
	{
		lg("\tautoconf client doesn't seem to be running, spawning one");  
		
		$forkit = 1;
	}
	
	
	if ($forkit)
	{
		 my $pid = fork(); 

		 if (not defined $pid)
		 {
			 die "out of resources? forking failed while spawning autoconf_client";
		 }
		 elsif ($pid == 0)
		 {
			 `/opt/bamt/autoconf_client`;
		 }
	}
}	
	

if (!defined($conf{'settings'}{'detect_defunct'}) || ($conf{'settings'}{'detect_defunct'} > 0))
{
	if ($verbose)
	{
	  print "look for defunct phoenix...\n";  
	}
	
	my %gps;
	my @ps = `ps axu | grep phoenix.py`;
	my $orphans = 0;
	
	foreach $l (@ps)
	{
			if ($l =~ /root\s+(\d+).+sh\s\-c\scd\s\/opt\/miners\/phoenix.+DEVICE=(\d+)/)
			{
					$gps{$2} = $1;
			}
			elsif ($l =~ /root\s+(\d+).+\/usr\/bin\/python\s\.\/phoenix\.py.+DEVICE=(\d+)/)
			{
	
					if (defined($gps{$2}))
					{
							$gps{$2} = $1;
					}
			}
			elsif ($l =~ /root\s+(\d+).+\[phoenix\.py\]\s\<defunct\>/)
			{
	
					my $wr = $1 - 1;
	
					my $found = 0;
	
					foreach $gpu(keys %gps)
					{
							if ($gps{$gpu} == $wr)
							{
									$found = 1;
									$gps{$gpu} = -1;
	
									lg("\tmarking gpu $gpu defunct");
									
							}
					}
	
					if ($found == 0)
					{
							# abandoned phoenix.. 
							lg("\torphan defunct phoenix at $1");
	
							
							$orphans++;
					}
	
			}
	
	}
	
	
	if ($orphans > 0)
	{
			lg("REBOOT due to $orphans orphan defunct phoenix!");
			
			sleep(1);
			`cp -f /var/log/bamt.log /live/image/BAMT/STATUS/`;
					
			exec('/sbin/coldreboot');
	}
	
	
	foreach $gpu (sort keys %gps)
	{
			if ( $gps{$gpu} == -1 )
			{
					lg("\twrapper with defunct phoenix process for device $gpu");
					
					if (-e "/live/image/BAMT/CONTROL/ACTIVE/noOCGPU$gpu")
					{
							my $gtime = (stat("/live/image/BAMT/CONTROL/ACTIVE/noOCGPU$gpu"))[9];
	
							# only if more than 5 min since we turned off O/C...	
							if ($gtime - time > 300)
							{
						
								# oc already disabled, yet it died.  disable entirely.
								if ($verbose)
								{
										print "\t\tdisabling mining on device $gpu\n";
								}
			
								open(F, ">/live/image/BAMT/CONTROL/ACTIVE/noGPU$gpu");
								print F "mother disabled this GPU due to a defunct phoenix process at " . localtime() . "\n";
								close(F);
							}
					}
					else
					{
							# disable OC for this GPU
							if ($verbose)
							{
									print "\t\tdisabling overclocking on device $gpu\n";
							}
	
							open(F, ">/live/image/BAMT/CONTROL/ACTIVE/noOCGPU$gpu");
							print F "mother disabled overclocking on this GPU due to a defunct phoenix process at " . localtime() . "\n";
							close(F);
	
					}
					lg("REBOOT due to defunct phoenix on gpu $gpu !");
			
					sleep(1);
					`cp -f /var/log/bamt.log /live/image/BAMT/STATUS/`;
					
					# down she goes..
					exec('/sbin/coldreboot');
			}
	}
	
}


if ($verbose)
{
  print "gathering GPU status...";  
}




my (@gpus) = &getFreshGPUData;

if ($verbose)
{
   print "done\n";
}


# status bcast
if (!defined($conf{settings}{do_bcast_status}) || ($conf{settings}{do_bcast_status} > 0) )
{
 if ($verbose)
 {
   print "broadcasting status\n";
 }
 &bcastStatus;
}

# status direct
if (defined($conf{settings}{do_direct_status}))
{
 if ($verbose)
 {
   print "sending status to " . $conf{settings}{do_direct_status} . "\n";
 }
 
 &directStatus($conf{settings}{do_direct_status});
}


# cgsnoop 
if ($conf{settings}{do_cgsnoop} == 1)
{
	if ($verbose)
	{
		print "forking for cgsnoop..\n";
	}
	
	my $pid = fork(); 

    if (not defined $pid)
    {
      die "out of resources? forking failed while starting cgsnoop";
    }
    elsif ($pid == 0)
    {
    	exec("/opt/bamt/cgsnoop");
    	exit(0);
    }
}


# config mgmt

if ($conf{settings}{do_manage_config})
{
 if ($verbose)
 {
   print "config mgmt.. snapshot config.. ";
 }
 

 # gather checksums
 my $origcksum = `/usr/bin/cksum /etc/bamt/*`; 

 if ($verbose)
 {
   print "chksum $origcksum\n";
   print "running update cmd...";
 }


 # sync config
 my $res = `$conf{settings}{config_update_cmd}`;

 if ($verbose)
 {
   print "done\n";
 }

 # compare
 my $newcksum = `/usr/bin/cksum /etc/bamt/*`;
 
 if ($verbose)
 {
   print "new chksum $newcksum\n";
 }


 if ($newcksum ne $origcksum)
 {
  if ($verbose)
  {
   lg("managed config changed, restarting mining to apply new config");
  }

  # restart mining

  &stopMining;

  sleep(2);

  &startMining;  

 }

}


# check for munin db in future
if (-e '/tmp/munin/db/'. ${$conf{settings}}{miner_id} )
{
 my $gtime = (stat('/tmp/munin/db/' .  ${$conf{settings}}{miner_id}))[9];

 if ($gtime - time > 300)
 {
	 if ($verbose)
	 {
		 print "removing /tmp/munin/db/${$conf{settings}}{miner_id} from the future\n";
	 }
	 `rm -Rf "/tmp/munin/db/${$conf{settings}}{miner_id}"`;
 }
}	 	



# monitoring

my $cmd = "";

if ($momtmp{mining} == 1)
{
 
 my %gpufaults;


 if ($verbose)
 {
  print "checking GPU health...\n";
 }

 
 # check cards for out of bounds
 for (my $k = 0;$k < @gpus;$k++)
 {
	  if ($verbose)
	  {
		print "\tcheck GPU $k...\n";
	  }
	
	 
	  my %gc = %{$conf{"gpu" . $k}};
	
	  my %mc = %{$momtmp{"gpu" . $k}};


	  # not mining at all detector for desktop bg
	  if ((! ${@gpus[$k]}{hashrate}) && (! $gc{disabled}))
	  {
			if ($verbose)
			{
				print "\t\tGPU $k is enabled but not mining\n";
			}
			
			$gpufaults{$k} = 1;
	  }

	  # the rest are only if card is monitored
	 
	  if ($conf{settings}{do_monitor})
	  {
	
		  for my $i (sort keys %{@gpus[$k]})
		  {
		  	  if ($verbose)
		  	  {
		  	  	  print "\t\t$i = " . ${$gpus[$k]}{$i} . "\n";
		  	  }
		  	  
			  if ($i =~ /^fault_(.+)_(hi|lo)$/)
			  {
			  	  if (defined($gpufaults{$k}))
			  	  {
			  	  	  $gpufaults{$k}++;
			  	  }
			  	  else
			  	  {
			  	  	  $gpufaults{$k} = 1;
			  	  }
			  	  
			  	  my $abovebelow = "above";
			  	  my $minmax = "maximum";
			  	  if ($2 eq 'lo')
			  	  {
			  	  	  $abovebelow = "below";
			  	  	  $minmax = "minimum";
			  	  }
			  	  
			  	  my @parts = split(/\|/, ${$gpus[$k]}{$i} );
			  	  
				  doAlert("GPU $k parameter '$1' is currently " . $parts[1] . ", $abovebelow the $minmax of " . $parts[0]);
			  }  
		  }
		
	   }

	   $momtmp{"gpu" . $k}{last_accepted} = ${@gpus[$k]}{shares_accepted}; 
 }



 for (my $k = 0;$k < @gpus;$k++)
 {

  if ($gpufaults{$k} && ( $momtmp{"gendesktop"} > -1) )
  {
   # 2 faults before you're out
   if ($momtmp{"fault_gpu$k"})
   {
    $cmd .= " $k";

    if ($verbose)
    {
     print "\tfaulting GPU $k\n";
    }

   }
   else
   {
    if ($verbose)
    {
     print "\tstrike 1 for GPU $k\n";
    }

    $momtmp{"fault_gpu$k"} = 1;
   }
  }
  else
  {
   # gpu ok, reset fault count
   
   if ($momtmp{"fault_gpu$k"} > 0)
   {
   		# recovery..
   		 doRecovery("GPU $k has returned to normal operation.");
   }
   $momtmp{"fault_gpu$k"} = 0;
  }
 }
}
elsif ($verbose)
{
 print "mining not started?, gpu monitoring skipped for now\n";
}



if (!defined(${$conf{'settings'}}{'do_gendesktopbg'}) || (${$conf{'settings'}}{'do_gendesktopbg'} != 0))
{
  if ($verbose)
  {
   print "refreshing desktop bg..\n";
  }


  if (! $cmd eq "")
  {
   # fault, always gen and reset gen counter
   `/opt/bamt/gendesktopbg err $cmd`;
   $momtmp{"gendesktop"} = 0; 
  }
  else
  {
   # no fault, gen every min for 5 after event/boot, then every 3
   $momtmp{"gendesktop"}++;
 
   if (($momtmp{"gendesktop"} < 5) || ($momtmp{"gendesktop"} > 7))
   {

   	# is mining disabled
   	if (-e '/tmp/noMine')
   	{
   		my $title = "Mining has been temporarily disabled because no usable GPUs are detected.";
   		
   		my $et = "On every boot, BAMT uses the aticonfig tool to scan for installed AMD/ATI GPUs.\n";
   		$et .= "If none are found, mining is temporarily disabled and a generic tool is used to\n";
   		$et .= "generate a safe configuration designed to show you the desktop regardless of\n";
   		$et .= "graphics hardware.  This failsafe configuration cannot be used to mine.";

		
		if ($verbose)
		{
			print "\n$title\n\n$et\n\n";
		}
			
   		`/opt/bamt/gendesktopbg fail '$title' '$et'`;
   	}
   	elsif (-e '/live/image/BAMT/CONTROL/ACTIVE/noMine')
   	{
   		my $title = "Mining has been disabled by the offline control interface";
   		  my $et = "Because the file /live/image/BAMT/CONTROL/ACTIVE/noMine is present, BAMT will\n";
   		  $et .= "not start the mining processes on this rig (if you access the USB key using a\n";
   		  $et .= "Windows machine or other system reading the FAT partition, the path is just\n";
   		  $et .= "BAMT/CONTROL/ACTIVE).";
			
   		if ($verbose)
		{
			print "\n$title\n\n$et\n\n";
		}  
   		    
   		`/opt/bamt/gendesktopbg warn '$title' '$et'`;
   	}
   	else
   	{
		# do we have anything configured
	
		my $gpuc = 0;
		for (my $k = 0;$k < @gpus;$k++)
		{
		 if ( !defined($conf{"gpu$k"}{disabled}) || ($conf{"gpu$k"}{disabled} == 0))
		 {
		  $gpuc++;
		 } 
		} 	
	
		if ($gpuc > 0)
		{
		 `/opt/bamt/gendesktopbg`;
	
		 if ($momtmp{"gendesktop"} > 7)
		 {
		  $momtmp{"gendesktop"} = 5;
		 }
		}
		else
		{
		 `/opt/bamt/gendesktopbg noconf`;
		 $momtmp{"gendesktop"} = 2;	 
		}
	}
   }
  }
}

$momtmp{'lastrun'} = time;

if ($recovery)
{
	if (defined($conf{settings}{smtp_host}) && defined($conf{settings}{smtp_to}))
	{
		lg("sending recovery alert email");
  	  	
		my $noalerts = $rmsg =~ tr/\n//;
		my $subject = "";
		
		if ($noalerts > 1)
		{
			$subject = $noalerts . " GPUs have recovered";
		}
		else
		{
			$subject = "A GPU has recovered";
		}
		
		$subject .= " at " . $conf{settings}{miner_id} . " (" . $conf{settings}{miner_loc} . ")";
		
		if (defined($conf{settings}{smtp_subject}))
		{
		   $subject = $conf{settings}{smtp_subject};
		}
  	  
  	  
		$rmsg = "\n" . $rmsg;
		&sendAnEmail($subject, $rmsg);
	}
	elsif ($verbose)
	{
		print "cannot send recovery alert email, smtp_host and/or smtp_to not specified\n";
	}
		
}


unless ($msg eq "")
{
 if (defined($conf{settings}{smtp_host}) && defined($conf{settings}{smtp_to}))
 {
 
  my $min_wait = 300;
  
  if (defined($conf{settings}{smtp_min_wait}))
  {
	$min_wait = $conf{settings}{smtp_min_wait};
  }
  
  # dont spam alerts
  if (time - $momtmp{'last_alert_email'} > $min_wait)
  {
  	  	lg("sending fault alert email");
  	  	
  	  
  	  	my $noalerts = $msg =~ tr/\n//;
		my $subject = "";
		
		if ($noalerts > 1)
		{
			$subject = $noalerts . " faults";
		}
		else
		{
			$subject = "1 fault";
		}
		
		$subject .= " at " . $conf{settings}{miner_id} . " (" . $conf{settings}{miner_loc} . ")";
		
		if (defined($conf{settings}{smtp_subject}))
		{
		   $subject = $conf{settings}{smtp_subject};
		}
  	  
  	  
		$msg = "\nCurrent faults:\n\n" . $msg;
		
		if (!defined($conf{settings}{smtp_include_details}) || ($conf{settings}{smtp_include_details} != 0) )
		{
			if ($verbose)
			{	
				print "\tinclude details...\n";
			}
			
			$msg .= "\n\nCurrent system status:\n";
			
			for (my $k = 0;$k < @gpus;$k++)
			{
				$msg .= "\n\tGPU $k\n\n";
				my %gc = %{$gpus[$k]};
				
				foreach $k (sort keys %gc)
				{
					if ($k =~ /^fault.*/)
					{
						$msg .= "\t!\t$k = " . $gc{$k} . "\n";
					}
					else
					{
						$msg .= "\t\t$k = " . $gc{$k} . "\n";
					}
				}
				
			}
		}
  	  
		
		&sendAnEmail($subject, $msg);
	
  }
  else
  {
   if ($verbose)
   {
    print "supressing fault alert email, last alert sent too recently (" . (time - $momtmp{'last_alert_email'}) . " seconds ago)\n";
   }
  }
 }
 elsif ($verbose)
 {
    lg("cannot send fault alert email, smtp_host and/or smtp_to not specified");
 }
}




&putMomTmp(%momtmp);

# the end


sub sendAnEmail
{
	my ($subject,$msg) = @_;
	
	my $host = $conf{settings}{smtp_host};
	my $to = $conf{settings}{smtp_to};
	
	my $from = '"BAMT - ' . $conf{settings}{miner_id} . '" <noreply@bamt.groupunix.com>';
	
	if (defined($conf{settings}{smtp_from}))
	{
	   $from = $conf{settings}{smtp_from};
	}
	

	
	my $port = 25;
	
	if (defined($conf{settings}{smtp_port}))
	{
	   $port = $conf{settings}{smtp_port};
	}
	
	my $tls = 0;
	
	if (defined($conf{settings}{smtp_tls}))
	{
	   $tls = $conf{settings}{smtp_tls};
	}
	
	my $ssl = 0;
	
	if (defined($conf{settings}{smtp_ssl}))
	{
	   $ssl = $conf{settings}{smtp_ssl};
	}
	
	# add full gpu details
	
	
	my $email = Email::Simple->create(
	header => [
	  To      => $to,
	  From    => $from,
	  Subject => $subject,
	],
	
	body => $msg,
	);
	
	if ($verbose)
	{
			print "\tto '$to' ...\n";
	}
	
	if (($tls == 1) && defined($conf{settings}{smtp_auth_user}) && defined($conf{settings}{smtp_auth_pass}))
	{
		if ($verbose)
		{
			print "\ttls, auth...\n";
		}
	
		$transport = Email::Sender::Transport::SMTP::TLS->new(
			host => $host,
			port => $port,
			username => $conf{settings}{smtp_auth_user},
			password => $conf{settings}{smtp_auth_pass},
			helo => 'bamt.groupunix.com',
		);
	}
	elsif (defined($conf{settings}{smtp_auth_user}) && defined($conf{settings}{smtp_auth_pass}))
	{
		if ($verbose)
		{
			print "\tno tls, sasl auth, ssl = $ssl...\n";
		}
		
		$transport = Email::Sender::Transport::SMTP->new(
			host => $host,
			port => $port,
			helo => 'bamt.groupunix.com',
			ssl => $ssl,
			sasl_username => $conf{settings}{smtp_auth_user},
			sasl_password => $conf{settings}{smtp_auth_pass},
			
		);
	}
	else
	{
		if ($verbose)
		{
			print "\tno tls, no auth, ssl = $ssl...\n";
		}
		
		$transport = Email::Sender::Transport::SMTP->new(
			host => $host,
			port => $port,
			helo => 'bamt.groupunix.com',
			ssl => $ssl,
			
		);
	}
		
	
	
	
	try 
	{
		if ($verbose)
		{	
			print "\tsending...\n";
		}
		
		sendmail($email, { transport => $transport });
		$momtmp{'last_alert_email'} = time;
		
		if ($verbose)
		{	
			print "\tcomplete\n";
		}
	
	} 
	catch 
	{
		if ($verbose)
		{
			print "error sending alert email: $!\n";
		}
	};
}



sub doAlert
{
 my ($txt) = @_;

 &blog($txt);
 
 if ($verbose == 1)
 {
   print "$txt\n";
 }

 $msg .= $txt . "\n";
 $alerts++;
}

sub doRecovery
{
 my ($txt) = @_;

 &blog($txt);
 
 if ($verbose == 1)
 {
   print "$txt\n";
 }

 $rmsg .= $txt . "\n";
 $recovery++;
 
}

sub lg
{
	my ($msg) = @_;
	
	&blog($msg);
	
	if ($verbose)
	{
		print "$msg\n"; 
	}
	
}

