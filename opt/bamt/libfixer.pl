#!/usr/bin/perl

#    This file is part of BAMT.
#
#    BAMT is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    BAMT is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with BAMT.  If not, see <http://www.gnu.org/licenses/>.


sub getFixPaths
{
 my $uname = `uname -a`;

 my $fixurl = 'http://sapinski.com/bamt/fix5';
 my $ldlibpath = '/opt/AMD-APP-SDK-v2.4-lnx32/lib/x86/';

 if ($uname =~ /amd64.*x86_64/)
 {
  $fixurl = 'http://sapinski.com/bamt/fix64';
  $ldlibpath = '/opt/AMD-APP-SDK-v2.4-lnx64/lib/x86_64/';
 }

 my $conf = &getConfig;
 %conf = %{$conf};
 
 if (defined($conf{settings}{fix_url}))
 {
 	 $fixurl = $conf{settings}{fix_url};
 }
 
 return($fixurl,$ldlibpath);
}


sub getFixLevel
{
	my $res = "none";
	
	if (-e "/opt/bamt/fix.history")
	{
	 	 open(F,"/opt/bamt/fix.history");
	 	 
	 	 while(<F>) 
	 	 {
	 	 	 chomp;
	 	 	 
	 	 	 if (/(.+),(\d+),install/)
	 	 	 {
	 	 	 	 $res = $1;
	 	 	 }
	 	 };
	 	 	 	 
	 	 close F;
	}
	
	return($res);
}


sub checkFixInstalled
{
 my ($fixfile) = @_;

 #inefficient as hell but whatever

 if (-e "/opt/bamt/fix.history")
 {
  open(F,"/opt/bamt/fix.history");
  
  if (grep{/$fixfile/} <F>)
  {
        close F; 
	return(1);
  }
  close F;
 }
 return(0);
}


sub getLatestFixLevel
{
	my ($fixurl,$ldlibpath) = &getFixPaths;
	
	my $res = "not available";
	
	use LWP::Simple;
	
	my $curfix = get $fixurl . '/fix';
	
	my (@cur) = split(/\n/,$curfix);
		
	foreach my $f (@cur)
	{
		if ($f =~ /.+?\.t.+/)
		{
			$res = $f;
		}
	}
	
	
	return($res);
}


sub checkFixesAvailable
{
	my ($fixurl,$ldlibpath) = &getFixPaths;
	
	my $fixins = 0;
	
	my $curfix = get $fixurl . '/fix';
	if (defined($curfix))
	{
		my (@cur) = split(/\n/,$curfix);
		
		foreach $fix (@cur)
		{
			if (!&checkFixInstalled($fix))
			{
				$fixins++;
			}
		}
	}	
	
	
	return($fixins);
}

1;
